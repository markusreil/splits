package com.mreil.splits.testutil.factory;

import com.mreil.splits.model.Participant;
import com.mreil.splits.model.SplitWeight;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class ParticipantTestFactory {
    public Participant newParticipant() {
        return Participant.builder()
                .name(RandomStringUtils.randomAlphanumeric(RandomUtils.nextInt(4, 12)))
                .shortName(RandomStringUtils.randomAlphanumeric(RandomUtils.nextInt(1, 3)))
                .defaultSplitWeight(SplitWeight.partsOfTotal(RandomUtils.nextInt(1, 3)))
                .build();
    }
}
