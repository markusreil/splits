package com.mreil.splits.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.math.BigDecimal;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PurchasedItem {
    @NonNull
    private String description;

    /**
     * Who paid how much?
     */
    @NonNull
    private Map<Participant, BigDecimal> amountPaid;

    /**
     * Who is participating in the purchase?
     */
    @NonNull
    private Map<Participant, SplitWeight> participantSplit;
}
