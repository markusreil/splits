package com.mreil.splits.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.math.BigDecimal;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CalculatedItem {
    @NonNull
    PurchasedItem purchasedItem;

    /**
     * The amount attributed to each participant based on the split.
     */
    @NonNull
    Map<Participant, BigDecimal> participantPart;

    /**
     * The total amount including the amount that was paid by each participant.
     */
    @NonNull
    Map<Participant, BigDecimal> participantTotal;
}
