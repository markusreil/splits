package com.mreil.splits.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Participant {
    @NonNull
    private UUID uuid;

    @NonNull
    private String name;

    @NonNull
    private String shortName;

    @NonNull
    private SplitWeight defaultSplitWeight;
}
