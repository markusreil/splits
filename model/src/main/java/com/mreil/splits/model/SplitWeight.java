package com.mreil.splits.model;

import lombok.ToString;

public class SplitWeight implements Comparable<SplitWeight> {
    public static final SplitWeight ONE_PART = new PartsOfTotal(1);

    public static final SplitWeight DEFAULT = ONE_PART;

    public static SplitWeight partsOfTotal(int parts) {
        return new PartsOfTotal(parts);
    }

    @ToString
    private static final class PartsOfTotal extends SplitWeight {
        private final int parts;

        public PartsOfTotal(int parts) {
            this.parts = parts;
        }

        public int getParts() {
            return parts;
        }

        @Override
        public int compareTo(SplitWeight o) {
            return 0;
        }
    }
}
