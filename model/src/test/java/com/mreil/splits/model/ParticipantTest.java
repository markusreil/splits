package com.mreil.splits.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.slf4j.MDC;

import java.util.UUID;

import static org.junit.Assert.*;

@Slf4j
public class ParticipantTest {

    @Test
    public void testParticipant() throws JsonProcessingException {
        Participant p = Participant.builder()
                .uuid(UUID.randomUUID())
                .name("name")
                .shortName("n")
                .defaultSplitWeight(SplitWeight.ONE_PART)
                .build();

        MDC.put("a", "b");
        log.info(p.toString() + "\n" + "newline" + new ObjectMapper().writeValueAsString(p));
    }

}
