package com.mreil.splits.service;

import com.google.common.collect.ImmutableMap;
import com.mreil.splits.model.Participant;
import com.mreil.splits.model.PurchasedItem;
import com.mreil.splits.testutil.factory.ParticipantTestFactory;
import com.sun.java.swing.plaf.windows.TMSchema;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;

import java.math.BigDecimal;

public class ItemCalculationServiceTest {
    ParticipantTestFactory pf = new ParticipantTestFactory();
    ItemCalculationService ics = new ItemCalculationService();

    @Test
    public void testSimple() {
        Participant p1 = pf.newParticipant();
        Participant p2 = pf.newParticipant();
        Participant p3 = pf.newParticipant();

        PurchasedItem item = PurchasedItem.builder()
                .amountPaid(ImmutableMap.of(p1, BigDecimal.valueOf(RandomUtils.nextDouble(0, 100))))
                .participantSplit(ImmutableMap.<Participant, BigDecimal>builder()
                        .put(p1, p1.getDefaultSplitWeight())
                        .put(p2, p2.getDefaultSplitWeight())
                        .put(p3, p3.getDefaultSplitWeight())
                                .build())
                .build();

        ics.calculateItem(item, )
    }
}
